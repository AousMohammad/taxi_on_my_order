// import 'package:app/driver/DriverFree.dart';
// import 'package:app/customer/StartingPosition.dart';
// import 'package:app/model/user/Response.dart';
import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
// import 'Auth.dart';

class SignUp extends StatefulWidget {
  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  // Response user;
  late int type;

  final _formKey = GlobalKey<FormState>();

  bool customerSelected = false;
  bool captainSelected = false;
  bool isPasswordVisible = false;
  bool isConfirmPasswordVisible = false;

  TextEditingController _userNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _userNameController.dispose();
    _emailController.dispose();
    _phoneNumberController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
          decoration: const BoxDecoration(
              gradient: LinearGradient(colors: [Colors.blue, Colors.blueGrey])),
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: Form(
                key: _formKey,
                child: Column(children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: Container(
                      height: 95,
                      width: 300,
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(
                              colors: [Colors.blue, Colors.blueGrey]),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                spreadRadius: 3,
                                color: Colors.black12)
                          ],
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(200),
                              bottomRight: Radius.circular(200))),
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 30, left: 50),
                        child: Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: const [
                              Text(
                                'Let\'s',
                                style: TextStyle(
                                    fontSize: 23,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                    shadows: [
                                      Shadow(
                                          color: Colors.black45,
                                          offset: Offset(1, 1),
                                          blurRadius: 5)
                                    ]),
                              ),
                              Text(
                                ' Register',
                                style: TextStyle(
                                    fontSize: 23,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue,
                                    shadows: [
                                      Shadow(
                                          color: Colors.black45,
                                          offset: Offset(1, 1),
                                          blurRadius: 5)
                                    ]),
                              ),
                            ]),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      controller: _userNameController,
                      validator: (val) {
                        return val!.isEmpty ? "Write the UserName " : null;
                      },
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: const InputDecoration(
                          prefixIconConstraints: BoxConstraints(minWidth: 45),
                          prefixIcon: Icon(
                            Icons.person,
                            color: Colors.white70,
                            size: 22,
                          ),
                          border: InputBorder.none,
                          hintText: 'Enter UserName',
                          hintStyle:
                          TextStyle(color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      controller: _emailController,
                      validator: (val) {
                        return val!.isEmpty ? "Write the Email " : null;
                      },
                      keyboardType: TextInputType.emailAddress,
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: const InputDecoration(
                          prefixIconConstraints: BoxConstraints(minWidth: 45),
                          prefixIcon: Icon(
                            Icons.email,
                            color: Colors.white70,
                            size: 22,
                          ),
                          border: InputBorder.none,
                          hintText: 'Enter Email',
                          hintStyle:
                          TextStyle(color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      controller: _phoneNumberController,
                      validator: (val) {
                        return val!.isEmpty ? "Write the PhoneNumber " : null;
                      },
                      keyboardType: TextInputType.number,
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      decoration: const InputDecoration(
                          prefixIconConstraints: BoxConstraints(minWidth: 45),
                          prefixIcon: Icon(
                            Icons.phone,
                            color: Colors.white70,
                            size: 22,
                          ),
                          border: InputBorder.none,
                          hintText: 'Enter PhoneNumber',
                          hintStyle:
                          TextStyle(color: Colors.white60, fontSize: 14.5),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38)),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      controller: _passwordController,
                      validator: (val) {
                        return val!.isEmpty ? "Write the Password " : null;
                      },
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      obscureText: isPasswordVisible ? false : true,
                      decoration: InputDecoration(
                          prefixIconConstraints: const BoxConstraints(minWidth: 45),
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.white70,
                            size: 22,
                          ),
                          suffixIconConstraints:
                          const BoxConstraints(minWidth: 45, maxWidth: 46),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                isPasswordVisible = !isPasswordVisible;
                              });
                            },
                            child: Icon(
                              isPasswordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.white70,
                              size: 22,
                            ),
                          ),
                          border: InputBorder.none,
                          hintText: 'Enter Password',
                          hintStyle:
                          const TextStyle(color: Colors.white60, fontSize: 14.5),
                          enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38)),
                          focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70))),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30)
                        .copyWith(bottom: 10),
                    child: TextFormField(
                      validator: (val) {
                        return val!.isEmpty
                            ? "Write the Confirm Password "
                            : null;
                      },
                      style: const TextStyle(color: Colors.white, fontSize: 14.5),
                      obscureText: isConfirmPasswordVisible ? false : true,
                      decoration: InputDecoration(
                          prefixIconConstraints: const BoxConstraints(minWidth: 45),
                          prefixIcon: const Icon(
                            Icons.lock,
                            color: Colors.white70,
                            size: 22,
                          ),
                          suffixIconConstraints:
                          const BoxConstraints(minWidth: 45, maxWidth: 46),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                isConfirmPasswordVisible =
                                !isConfirmPasswordVisible;
                              });
                            },
                            child: Icon(
                              isConfirmPasswordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.white70,
                              size: 22,
                            ),
                          ),
                          border: InputBorder.none,
                          hintText: 'Confirm Password',
                          hintStyle:
                          const TextStyle(color: Colors.white60, fontSize: 14.5),
                          enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white38)),
                          focusedBorder: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white70))),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              customerSelected = true;
                              captainSelected = false;
                              type = 0;
                            });
                          },
                          child: Row(
                            children: [
                              Container(
                                  height: 20,
                                  width: 20,
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.only(right: 10),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border:
                                      Border.all(color: Colors.white60)),
                                  child: customerSelected
                                      ? Container(
                                    margin: const EdgeInsets.all(4),
                                    decoration: const BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white70),
                                  )
                                      : const SizedBox()),
                              const Text('Customer',
                                  style: TextStyle(
                                      color: Colors.white70, fontSize: 17))
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              captainSelected = true;
                              customerSelected = false;
                              type = 1;
                            });
                          },
                          child: Row(children: [
                            Container(
                                height: 20,
                                width: 20,
                                alignment: Alignment.center,
                                margin: const EdgeInsets.only(right: 10),
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(color: Colors.white60)),
                                child: captainSelected
                                    ? Container(
                                  margin: const EdgeInsets.all(4),
                                  decoration: const BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: Colors.white70),
                                )
                                    : const SizedBox()),
                            const Text('Captain',
                                style: TextStyle(
                                    color: Colors.white70, fontSize: 17))
                          ]),
                        ),
                      ]),
                  const SizedBox(height: 30),
                  GestureDetector(
                    /*onTap: () {
                      if (_formKey.currentState.validate()) {
                        register();
                      } else {
                        Provider.of<Auth>(context, listen: false).logout();
                      }
                    },*/
                    child: Container(
                      height: 70,
                      width: 250,
                      margin: const EdgeInsets.symmetric(horizontal: 30),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4,
                                color: Colors.black12.withOpacity(.2),
                                offset: const Offset(2, 2))
                          ],
                          gradient: const LinearGradient(
                              colors: [Colors.blue, Colors.blueGrey])),
                      child: Text('Sign  Up',
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8),
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                ]),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void register() async {
    /*Map users = {
      'name': _userNameController.text,
      'email': _emailController.text,
      'phone_number': _phoneNumberController.text,
      'password': _passwordController.text,
      'type': type,
    };
    Response response =
    await Provider.of<Auth>(context, listen: false).register(users: users);
    if (response.status && captainSelected) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => DriverFree(),
          ));
    }
    if (response.status && customerSelected) {
      Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => StartingPosition(),
          ));
    }*/
  }
}
